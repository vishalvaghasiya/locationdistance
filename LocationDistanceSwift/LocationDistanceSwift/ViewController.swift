//
//  ViewController.swift
//  LocationDistanceSwift
//
//  Created by VKGraphics on 04/05/17.
//  Copyright © 2017 VKGraphics. All rights reserved.
//

import UIKit
import CoreLocation
class ViewController: UIViewController {

    @IBOutlet var distanceButton: UIButton!
    @IBOutlet var distanceLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        distanceButton.layer.shadowRadius = 3.0
        distanceButton.layer.shadowOpacity = 0.4
        distanceButton.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(3.0))
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func getDistanceButtonTapped(_ sender: UIButton) {
        let  myLocation = CLLocation(latitude: 21.225351, longitude: 72.8786284)
        let location1 = CLLocation(latitude: 21.2142918, longitude: 72.8586776)
        let location2 = CLLocation(latitude: 21.2299611, longitude: 72.8882792)
        let location3 = CLLocation(latitude: 21.2299611, longitude: 72.8127235)
        let location4 = CLLocation(latitude: 21.183485, longitude: 72.8110773)
        let location5 = CLLocation(latitude: 21.2055402, longitude: 72.8665673)
        let location6 = CLLocation(latitude: 21.2237895, longitude: 72.8640781)
        let location7 = CLLocation(latitude: 21.2374234, longitude: 72.8752928)
        
        let LocationArray = [location1,location2,location3,location4,location5,location6,location7]
        
        for findDistance in LocationArray {
            let distance = myLocation.distance(from: findDistance) / 1000;
            if distance > 2.0 {
                print("The distance to myLocation is %.01fkm \(distance)")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

